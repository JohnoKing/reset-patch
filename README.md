This simple patch improves the performance of reset(1) dramatically, but kills compatibility with real terminals.
Only use this patch with terminal emulators.
